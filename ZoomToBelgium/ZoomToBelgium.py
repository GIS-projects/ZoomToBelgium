﻿# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ZoomToBelgium
 A button to zoom to any of the Belgian administrative boundaries in QGIS.
                              -------------------
        begin                : 2017-09-19
        copyright            : (C) 2017 by Michel Stuyts
        email                : info@stuyts.xyz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import QAction
from .resources3 import *
from .ZoomToBelgium_dialog import ZoomToBelgiumDialog
import os.path,unicodedata
from collections import OrderedDict
from qgis.core import *
import unicodedata


class ZoomToBelgium:
    def tr(self, message):
        return QCoreApplication.translate('ZoomToBelgium', message)

    def __init__(self, iface):
        self.dlg = ZoomToBelgiumDialog()
        global contour_name
        global contour_xmax
        global contour_xmin
        global contour_ymax
        global contour_ymin
        
        """
        ID's:
            * 1-899: names of municipalities in original language
            * 900-999 names of provinces in original language
            * 1000-1899: names of municipalities in translated language or second official language
            * 1900-1999: names of provinces in translated language
            * 2000-2999: names of districts
            * 3000-3999: communities and regions
			* negative: Belgium (country)
        """
        
        contourlayer = QgsVectorLayer(os.path.join(os.path.dirname(__file__),"osm_boundaries_bbox.geojson"), "boundaries", "ogr")

        def strip_accents(s):
           return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
        
        def get_name(f):
            return strip_accents(f['name'])
        
        contours = sorted(contourlayer.getFeatures(), key=get_name)
        
        contour_name=OrderedDict()
        contour_xmax=OrderedDict()
        contour_xmin=OrderedDict()
        contour_ymax=OrderedDict()
        contour_ymin=OrderedDict()
        

        for contour in contours:
            contour_name[int(contour['zoom_id'])]=contour['name']
            geometry=contour.geometry().boundingBox()
            
            contour_xmax[int(contour['zoom_id'])]=geometry.xMaximum()
            contour_xmin[int(contour['zoom_id'])]=geometry.xMinimum()
            contour_ymax[int(contour['zoom_id'])]=geometry.yMaximum()
            contour_ymin[int(contour['zoom_id'])]=geometry.yMinimum()
       
        global combovalues, locale
        combovalues=[]
        combovaluesutf8=[]
        for key,value in contour_name.items():
            combovalues.append(value.encode('utf-8').strip())
        self.dlg.comboGemeentes.clear()
        for combovalue in combovalues:
            combovaluesutf8.append(combovalue.decode('utf-8'))
            
        self.dlg.comboGemeentes.addItems(combovaluesutf8)
        global selected_contour
        global setting
        setting = QSettings()
        self.iface = iface
        self.plugin_dir = os.path.dirname(__file__)
        # locale = QSettings().value('locale/userLocale')[0:2]
        locale = QSettings().value('locale/userLocale', QLocale().name())[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'ZoomToBelgium_{}.qm'.format(locale))
        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
        self.actions = []
        self.menu = self.tr(u'&ZoomToBelgium')
        self.toolbar = self.iface.addToolBar(u'ZoomToBelgium')
        self.toolbar.setObjectName(u'ZoomToBelgium')
        self.dlg.button_box.accepted.connect(self.savesettings)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=False,
        status_tip=None,
        whats_this=None,
        parent=None):

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)
        return action

    def add_action_toolbar(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=False,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        icon = QIcon(icon_path)
        actiontoolbar = QAction(icon, text, parent)
        actiontoolbar.triggered.connect(callback)
        actiontoolbar.setEnabled(enabled_flag)

        if status_tip is not None:
            actiontoolbar.setStatusTip(status_tip)

        if whats_this is not None:
            actiontoolbar.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(actiontoolbar)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                actiontoolbar)

        self.actions.append(actiontoolbar)
        return actiontoolbar

    def initGui(self):
        icon_path = ':/plugins/ZoomToBelgium/icon.png'
        settings_icon_path = ':/plugins/ZoomToBelgium/settings.png'
        self.add_action_toolbar(
            icon_path,
            text=self.tr(u'Zoom to the Selected Boundary'),
            callback=self.run,
            parent=self.iface.mainWindow())
        self.add_action(
            icon_path,
            text=self.tr(u'Zoom to the Selected Boundary'),
            callback=self.run,
            parent=self.iface.mainWindow())
        self.add_action(
            settings_icon_path,
            text=self.tr(u'Select Boundary'),
            callback=self.opensettings,
            parent=self.iface.mainWindow())

    def unload(self):
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&ZoomToBelgium'),
                action)
            self.iface.removeToolBarIcon(action)
        del self.toolbar

    def nomunicipalityselected(self):
        self.opensettings()

    def zoomtomunicipality(self, id):
        canvas = self.iface.mapCanvas()
        PROJECTsrs = canvas.mapSettings().destinationCrs()
        my_crs = QgsCoordinateReferenceSystem("EPSG:31370")
        canvas.setDestinationCrs(my_crs)

        selected_contour=setting.value("zoomtobelgium/municipality", 518)
        selectedcontour_int=int(selected_contour[0])
        if(selectedcontour_int>1000 and selectedcontour_int<2000):
            selectedcontour_int=selectedcontour_int-1000
        if(selectedcontour_int<0):
            xmax_contour=max(contour_xmax.values())
            xmin_contour=min(contour_xmin.values())
            ymax_contour=max(contour_ymax.values())
            ymin_contour=min(contour_ymin.values())
        else:
            xmax_contour=contour_xmax[selectedcontour_int]
            xmin_contour=contour_xmin[selectedcontour_int]
            ymax_contour=contour_ymax[selectedcontour_int]
            ymin_contour=contour_ymin[selectedcontour_int]
        buffer_x=(abs(xmax_contour-xmin_contour))/50
        buffer_y=(abs(ymax_contour-ymin_contour))/50
        buffer=max(buffer_x,buffer_y)
        xmax_setting=xmax_contour+buffer
        xmin_setting=xmin_contour-buffer
        ymax_setting=ymax_contour+buffer
        ymin_setting=ymin_contour-buffer

        canvas.setExtent(QgsRectangle(xmin_setting,ymin_setting,xmax_setting,ymax_setting))

        canvas.setDestinationCrs(PROJECTsrs)
        canvas.refresh()

    def opensettings(self):
        selected_contour=[]
        selected_contourtemp=setting.value("zoomtobelgium/municipality")

        if not selected_contourtemp:
            selected_contour.append(518)
        else:
            selected_contour.append(int(selected_contourtemp[0]))
        selected_contourindex=list(contour_name).index(int(selected_contour[0]))
        self.dlg.comboGemeentes.setCurrentIndex(selected_contourindex)
        introsubtext=self.tr(u'Select Boundary')
        introtext=u'<html><head/><body><p><span style=" font-size:16pt;">'+introsubtext+'</span></p></body></html>'
        osmsubtext=self.tr(u'The boundaries used to zoom to your preferred location are based on OpenStreetMap data')
        osmsubtextcontrib=self.tr(u'OpenStreetMap contributors')
        osmtext=u'<html><body><p style="font-size: 8pt;"><em>'+osmsubtext+' (&lt;a href=&quot;https://www.openstreetmap.org/copyright&quot; target=&quot;_blank&quot;&gt;© '+osmsubtextcontrib+'&lt;/a&gt;)</em></p></body></html>'
        self.dlg.intro.setText(introtext.replace("&lt;","<").replace("&gt;",">").replace("&quot;",'"'))
        self.dlg.osmcopyright.setText(osmtext.replace("&lt;","<").replace("&gt;",">").replace("&quot;",'"'))
        self.dlg.show()

    def savesettings(self):
        to_save_id=self.search_contour(contour_name,self.dlg.comboGemeentes.currentText().strip())
        setting.setValue("zoomtobelgium/municipality", to_save_id)
        self.zoomtomunicipality(to_save_id)

    def search_contour(self,list,search_contour):
       return [id for id,municipality in list.items() if municipality == search_contour]

    def run(self):
        selected_contour=setting.value("zoomtobelgium/municipality", 0)
        if selected_contour=="" or selected_contour==0 or selected_contour is None:
            self.nomunicipalityselected()
        else:
            self.zoomtomunicipality(selected_contour)
