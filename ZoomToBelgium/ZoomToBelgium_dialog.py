# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ZoomToBelgiumDialog
A button to zoom to any of the Belgian administrative boundaries in QGIS.
                             -------------------
        begin                : 2017-09-19
        author               : Michel Stuyts
        email                : info@stuyts.xyz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
from qgis.PyQt.QtCore import *
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QDialog

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'ZoomToBelgium_dialog_base.ui'))


class ZoomToBelgiumDialog(QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        super(ZoomToBelgiumDialog, self).__init__(parent)
        self.resize(QSize(400, 230).expandedTo(self.minimumSizeHint()))
        self.setWindowIcon(QIcon(":/plugins/ZoomToBelgium/icon.png"))
        try:
            self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint | Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        except AttributeError:
            self.setWindowFlags(self.windowFlags() & ~Qt.WindowType.WindowContextHelpButtonHint | Qt.WindowType.CustomizeWindowHint | Qt.WindowType.WindowTitleHint)
        self.setupUi(self)
