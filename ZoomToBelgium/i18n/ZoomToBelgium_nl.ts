<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl" sourcelanguage="en">
<context>
    <name>ZoomToBelgium</name>
    <message>
        <location filename="../ZoomToBelgium.py" line="96"/>
        <source>&amp;ZoomToBelgium</source>
        <translation>&amp;ZoomToBelgium</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="173"/>
        <source>Zoom to the Selected Boundary</source>
        <translation>Zoom naar de geselecteerde administratieve grens</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="183"/>
        <source>Select Boundary</source>
        <translation>Selecteer een administratieve grens</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="244"/>
        <source>The boundaries used to zoom to your preferred location are based on OpenStreetMap data</source>
        <translation>De grenzen die gebruikt worden om in te zoomen, zijn gebaseerd op OpenStreetMap data</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="245"/>
        <source>OpenStreetMap contributors</source>
        <translation>bijdragers OpenStreetMap</translation>
    </message>
</context>
</TS>
