<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>ZoomToBelgium</name>
    <message>
        <location filename="../ZoomToBelgium.py" line="96"/>
        <source>&amp;ZoomToBelgium</source>
        <translation>&amp;ZoomToBelgium</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="173"/>
        <source>Zoom to the Selected Boundary</source>
        <translation>Zoom à la limite administrative sélectionnée</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="283"/>
        <source>Select Boundary</source>
        <translation>Sélectionnez une limite administrative</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="244"/>
        <source>The boundaries used to zoom to your preferred location are based on OpenStreetMap data</source>
        <translation>Les limites administrative utilisées pour zoomer sont basées sur les données OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../ZoomToBelgium.py" line="245"/>
        <source>OpenStreetMap contributors</source>
        <translation>les contributeurs et contributrices OpenStreetMap</translation>
    </message>
</context>
</TS>
