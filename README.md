<h1 style="text-align: center;">ZoomToBelgium ![ZoomToBelgium](https://gitlab.com/GIS-projects/ZoomToBelgium/raw/master/ZoomToBelgium/icon.png)</h1>

<p style="text-align: center;">[English](#a-button-to-zoom-to-any-of-the-belgian-administrative-boundaries-in-qgis-3) - [Nederlands](#een-knop-om-naar-%C3%A9%C3%A9n-van-de-belgische-administratieve-grenzen-te-zoomen-in-qgis-3)</p>

----

## A button to zoom to any of the Belgian administrative boundaries in QGIS 3.
Since version 2.0, this plugin can only be used in QGIS 3.  QGIS 2 is no longer supported.

### How to install the plugin
This plugin is available in the official [QGIS plugin repository](https://plugins.qgis.org/plugins/ZoomToBelgium/). So you can [download and install](https://docs.qgis.org/latest/en/docs/training_manual/qgis_plugins/fetching_plugins.html) it from within QGIS.

### How to use this plugin
After you installed the plugin, a new button (![ZoomToBelgium](https://gitlab.com/GIS-projects/ZoomToBelgium/raw/master/ZoomToBelgium/icon.png)) wil appear in the toolbar. The first time you click on the button it will ask to which province, municipality or district you want to zoom. After you select a country, region, community, province, municipality or district, it will zoom to its extents. The next time you click on the button you longer have to select the administrative boundary and you will zoom to the previously selected country, region, community, province, municipality or district. If you want to set the button to a different country, region, community, province, municipality or district you can do so in the "Plugins" menu under ZoomTobelgium.

-----

## Een knop om naar één van de Belgische administratieve grenzen te zoomen in QGIS 3
Sinds versie 2.0, kan deze plugin alleen nog in QGIS 3 gebruikt worden. QGIS 2 wordt niet langer ondersteund.

### Hoe installeer je deze plugin
Deze plugin is ook beschikbaar in de [officiële QGIS plugin repository](https://plugins.qgis.org/plugins/ZoomToBelgium/). Je kan hem dus vanuit QGIS [downloaden en installeren](https://docs.qgis.org/latest/nl/docs/training_manual/qgis_plugins/fetching_plugins.html).

### Hoe gebruik je deze plugin
Nadat je de plugin geïnstalleerd hebt, verschijnt er een nieuwe knop in de knoppenbalk (![ZoomToBelgium](https://gitlab.com/GIS-projects/ZoomToBelgium/raw/master/ZoomToBelgium/icon.png)).  De eerste keer dat je op de knop klikt, wordt er gevraagd naar welke provincie, gemeente of district je wil inzoomen. Nadat je een land, gewest, gemeenschap, provincie, gemeente of district geselecteerd hebt, zal QGIS er naar zoomen.  De volgende keer dat je op de knop klikt, zal er ingezoomd worden naar dat land, gewest, gemeenschap,  provincie, die gemeente of dat district zonder dat je nog een administratieve grens moet selecteren. Als je de knop op een ander land, gewest, gemeenschap,  provincie, gemeente of district wil instellen kan je dat doen via het "Plugins" menu onder het item ZoomToBelgium.
